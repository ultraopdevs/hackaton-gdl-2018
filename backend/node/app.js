var express        = require('express'),
	bodyParser     = require('body-parser'),
	methodOverride = require('method-override'),
	morgan         = require('morgan'),
	restful        = require('node-restful'),
	mongoose       = restful.mongoose;
	// mongodb        = require('mongoose')
var app            = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(methodOverride());

// mongoose.connect("mongodb://localhost/resources");
mongoose.connect("mongodb://hackton:yolo@192.168.0.102:27017/hackton");

var rutaShema = mongoose.Schema(
	{
		nombre:String,
		fecha: { type: Date, default: Date.now },
		control    : [{
			nombre   : String,
			time     : Number,
			longitud : Number,
			latitud  : Number
		}],
		geometry   :  [{
			nombre   :  String,
			time     :  Number,
			longitud :  Number,
			latitud  :  Number
		}]
	},
	{
		collection:'rutas'
	}
);


var posicion = mongoose.Schema(
	{
		ruta : String,
		geometry:[]	
	},
	{
		collection:'posicion'
	}
);

var otracosa = mongoose.Schema(
	{
		ruta : String,
		geometry:[]	
	},
	{
		collection:'otracosa'
	}
);

// // API ROUTES -------------------
// var Rutas = app.resource = restful.model('rutas', rutaShema)
	// .methods(['put', 'delete']);

// Rutas.register(app, '/transportador/ws/rutas');

var mongoseRuta = mongoose.model('rutas', rutaShema);
var mongosePosicion = mongoose.model('posicion', posicion);

// get name ruta
app.get('/transportador/ws/rutas/:nombre', function (req, res) {
	mongoseRuta.find({"nombre":req.params.nombre}, function (err, ruta) {
		res.send(ruta);
		
	});
});

// POST /dashboard/resume/create
app.post('/transportador/ws/rutas', (req, res, next) => {
	console.log(next);

	mongoseRuta.create(req.body, (err, ruta) => {
		if (err) {
			var err = new Error("Error:" + err);
			err.status = 404;
			next(err);
		} else {
			res.send(ruta);
		}
	});
});

// POST /dashboard/resume/create
app.post('/transportador/ws/posicion', (req, res, next) => {
	console.log("posicion",req.body);
	mongosePosicion.create(req.body, (err, posicion) => {
		if (err) {
			var err = new Error("Error:" + err);
			err.status = 404;
			next(err);
		} else {
			res.send(["success",posicion]);
		}
	});
});

// POST /dashboard/resume/create
app.post('/transportador/ws/otracosa', (req, res, next) => {
	console.log("otracosa",req.body);
	otracosa.create(req.body, (err, otracosa) => {
		if (err) {
			var err = new Error("Error:" + err);
			err.status = 404;
			next(err);
		} else {
			res.send(["success",otracosa]);
		}
	});
});

// get name ruta
app.get('/transportador/ws/posicion/:nombre', function (req, res) {
	mongosePosicion.find({"ruta":req.params.nombre}, function (err, position) {
		res.send(position);
		
	});
});

app.listen(8081);
/*
	GET    /transportador/ws/rutas
	GET    /transportador/ws/rutas/:id
	POST   /transportador/ws/rutas
	PUT    /transportador/ws/rutas/:id
	DELETE /transportador/ws/rutas/:id
*/