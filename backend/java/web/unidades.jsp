<!DOCTYPE html>
<!-- saved from url=(0053)https://getbootstrap.com/docs/4.0/examples/dashboard/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>EL TRANSPORTADOR - Admin Panel</title>
        <!-- Bootstrap core CSS -->
        <link href="./dashboard/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./dashboard/dashboard.css" rel="stylesheet">
        <style type="text/css">/* Chart.js */
            @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
        </style>
        <script src="vendors/jquery/jquery-3.2.1.js" type="text/javascript"></script>
        <script>
            $(function () {
                $.ajax({
                    url: 'ws/unidad/pagina/1/30',
                    cache: false
                }).done(function (data) {
                    var template = $($('#unidad-template').html());
                    for (var i in data) {
                        var t = data[i];
                        var tl = template.clone(false);
                        for (var k in t) {
                            var n = '[data-' + k + "]";
                            tl.find(n).text(t[k]);
                        }
                        $('#unidad-table').append(tl);
                    }
                }).fail(function (a, b, c) {
                    console.log(a, b, c);
                });
            });
        </script>
    </head>

    <body>
        <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="https://getbootstrap.com/docs/4.0/examples/dashboard/#">Transporte Urbano Guaycura</a>
            <input class="form-control form-control-dark w-100" type="text" placeholder="Buscar" aria-label="Buscar">
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="https://getbootstrap.com/docs/4.0/examples/dashboard/#">SALIR</a>
                </li>
            </ul>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <%@include file="nav.jsp" %>

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
                        <h1 class="h2">Veh�culos</h1>
                        <div class="btn-toolbar mb-2 mb-md-0">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-secondary">Nuevo Veh�culo</button>
                                <button class="btn btn-sm btn-outline-secondary">Desactivar Veh�culo</button>
                                <button class="btn btn-sm btn-outline-secondary">Exportar</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm text-center">
                            <thead>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th>ID</th>
                                    <th>No. Econ�mico</th>
                                    <th>Capacidad</th>
                                    <th>Placas</th>
                                    <th>Modelo</th>
                                    <th>A�o</th>
                                    <th>Ultimo Mantenimiento</th>
                                    <th>P�liza Seguro</th>
                                    <th>Ruta</th>
                                </tr>
                            </thead>
                            <tbody id="unidad-table">
                            <template id="unidad-template">
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td data-idunidad></td>
                                    <td data-numeroEconomico></td>
                                    <td data-maximoPasajeros></td>
                                    <td data-placas></td>
                                    <td data-modelo></td>
                                    <td data-anio></td>
                                    <td data-ultimoMantenimiento></td>
                                    <td data-polizaSeguro></td>
                                    <td data-ruta></td>
                                </tr>
                            </template>
                            </tbody>
                        </table>
                    </div>
                </main>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./dashboard/popper.min.js.descarga"></script>
        <script src="./dashboard/bootstrap.min.js.descarga"></script>

        <!-- Icons -->
        <script src="./dashboard/feather.min.js.descarga"></script>
        <script>
            feather.replace();
        </script>
    </body>
</html>