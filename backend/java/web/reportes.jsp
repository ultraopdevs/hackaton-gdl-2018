<!DOCTYPE html>
<!-- saved from url=(0053)https://getbootstrap.com/docs/4.0/examples/dashboard/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>EL TRANSPORTADOR - Admin Panel</title>
        <!-- Bootstrap core CSS -->
        <link href="./dashboard/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./dashboard/dashboard.css" rel="stylesheet">
        <style type="text/css">/* Chart.js */
            @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
        </style>
        <script src="vendors/jquery/jquery-3.2.1.js" type="text/javascript"></script>
        <script>
            $(function () {
                $.ajax({
                    url: 'ws/reporte/pagina/1/30',
                    cache: false
                }).done(function (data) {
                    var template = $($('#reporte-template').html());
                    for (var i in data) {
                        var t = data[i];
                        var tl = template.clone(false);
                        for (var k in t) {
                            if (k === 'pasajeroIdpasajero') {
                                var n = '[data-idpasajero]';
                                tl.find(n).text(t[k].idpasajero);
                            } else if (k === 'unidadIdunidad') {
                                var n = '[data-ruta]';
                                tl.find(n).text(t[k].ruta);
                                var n = '[data-unidad]';
                                tl.find(n).text(t[k].numeroEconomico);
                            } else {
                                var n = '[data-' + k + "]";
                                tl.find(n).text(t[k]);
                            }
                        }
                        $('#reporte-table').append(tl);
                    }
                }).fail(function (a, b, c) {
                    console.log(a, b, c);
                });
            });
        </script>
    </head>

    <body>
        <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="https://getbootstrap.com/docs/4.0/examples/dashboard/#">Transporte Urbano Guaycura</a>
            <input class="form-control form-control-dark w-100" type="text" placeholder="Buscar" aria-label="Buscar">
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="https://getbootstrap.com/docs/4.0/examples/dashboard/#">SALIR</a>
                </li>
            </ul>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <%@include file="nav.jsp" %>

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
                        <h1 class="h2">Consumo de combustible</h1>
                        <div class="btn-toolbar mb-2 mb-md-0">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-secondary">Nuevo Reporte</button>
                                <button class="btn btn-sm btn-outline-secondary">Imprimir Reporte</button>
                                <button class="btn btn-sm btn-outline-secondary">Exportar</button>
                                <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                                    Seleccione Periodo
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm text-center">
                            <thead>
                                <tr>
                                    <th>No. Econ�mico</th>
                                    <th>Ruta</th>
                                    <th>Consumo Promedio de Combustible</th>
                                    <th>Kil�metraje Diario Promedio</th>
                                    <th>Rendimiento Combustible</th>
                                    <th>Desviaci�n</th>
                                    <th>Promedio de Viajes Diarios</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td>SEP Centro</td>
                                    <td>170 lts</td>
                                    <td>82 km</td>
                                    <td>2.0731 km/lt</td>
                                    <td>3.0120 km/lt</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td>SEP Centro</td>
                                    <td>170 lts</td>
                                    <td>82 km</td>
                                    <td>2.0731 km/lt</td>
                                    <td>3.0120 km/lt</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>001</td>
                                    <td>SEP Centro</td>
                                    <td>170 lts</td>
                                    <td>82 km</td>
                                    <td>2.0731 km/lt</td>
                                    <td>3.0120 km/lt</td>
                                    <td>6</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
                            <h1 class="h2">Puntualidad de Rutas</h1>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm text-center">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Ruta</th>
                                        <th>Unidad</th>
                                        <th>Checadas a Tiempo</th>
                                        <th>Checadas Fuera de Tiempo</th>
                                        <th>Eficiencia</th>
                                        <th>ID Transportador</th>
                                        <th>Nombre Transportador</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>11/02/2018</td>
                                        <td>SEP Contra</td>
                                        <td>754</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>25.00%</td>
                                        <td>0713</td>
                                        <td>Alfredo P�rez</td>
                                    </tr>
                                    <tr>
                                        <td>11/02/2018</td>
                                        <td>SEP Contra</td>
                                        <td>754</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>25.00%</td>
                                        <td>0713</td>
                                        <td>Alfredo P�rez</td>
                                    </tr>
                                    <tr>
                                        <td>11/02/2018</td>
                                        <td>SEP Contra</td>
                                        <td>754</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>25.00%</td>
                                        <td>0713</td>
                                        <td>Alfredo P�rez</td>
                                    </tr>
                                </tbody>
                            </table>
                            <h1 class="h2">Reportes de Usuarios</h1>
                            <div class="table-responsive">
                                <table class="table table-striped table-sm text-center">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Ruta</th>
                                            <th>Unidad</th>
                                            <th>Categor�a</th>
                                            <th>Descripci�n</th>
                                            <th>Usuario que reporta</th>
                                            <th>Validaciones Adicionales</th>
                                        </tr>
                                    </thead>
                                    <tbody id="reporte-table">
                                    <template id="reporte-template">
                                        <tr>
                                            <td data-fecha></td>
                                            <td data-ruta></td>
                                            <td data-unidad></td>
                                            <td data-tipo></td>
                                            <td data-comentario></td>
                                            <td data-idpasajero></td>
                                            <td data-validaciones></td>
                                        </tr>
                                    </template>
                                    </tbody>
                                </table>
                            </div>
                            </main>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./dashboard/popper.min.js.descarga"></script>
        <script src="./dashboard/bootstrap.min.js.descarga"></script>

        <!-- Icons -->
        <script src="./dashboard/feather.min.js.descarga"></script>
        <script>
            feather.replace();
        </script>
    </body>
</html>