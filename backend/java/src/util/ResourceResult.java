package util;

import java.util.Collection;

public abstract class ResourceResult<T> {

    protected String namedQueryRoot;

    /**
     * Retrieves representation of an instance
     *
     * @param id case management id
     * @return a collection
     */
    public abstract Collection<T> getAll(Integer id);

    /**
     * POST method for updating an instance
     *
     * @param id
     * @param content
     * @return generated key
     */
    public abstract String post(Integer id, T content);

    /**
     * PUT method for updating or creating an instance
     *
     * @param id
     * @param content representation for the resource
     */
    public abstract void put(Integer id, T content);

    /**
     * DELETE method for updating an instance
     *
     * @param superId
     * @param id
     */
    public abstract void delete(Integer superId, Integer id);

}
