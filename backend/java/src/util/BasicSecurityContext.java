package util;

import java.security.Principal;
import javax.ws.rs.core.SecurityContext;

class BasicSecurityContext implements SecurityContext {

    private final User user;
    private final boolean secure;

    public BasicSecurityContext(User user, boolean secure) {
        this.user = user;
        this.secure = secure;
    }

    @Override
    public Principal getUserPrincipal() {
        return new Principal() {
            @Override
            public String getName() {
                return user.getUsername();
            }
        };
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }

    @Override
    public boolean isSecure() {
        return secure;
    }

    @Override
    public boolean isUserInRole(String role) {
        return user.getRoles().contains(role);
    }
}
