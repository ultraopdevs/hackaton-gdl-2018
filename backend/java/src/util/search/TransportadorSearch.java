/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.search;

import model.Transportador;

/**
 *
 * @author diein
 */
public class TransportadorSearch {

    private Transportador transportador;

    public Transportador getTransportador() {
        return transportador;
    }

    public void setTransportador(Transportador transportador) {
        this.transportador = transportador;
    }
    
}
