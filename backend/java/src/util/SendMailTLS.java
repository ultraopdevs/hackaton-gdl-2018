package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

public class SendMailTLS {

    private static Properties email = null;

    public static String REGISTRO = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <title>Carrera CUPREX</title>\n"
            + "        <meta charset=\"UTF-8\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
            + "    </head>\n"
            + "    <body>\n"
            + "        <h3>Carrera CUPREX</h3>\n"
            + "        <p>\n"
            + "            Estimado <b>||PERSONA</b> gracias por registrarte a la carrera CUPREX.\n"
            + "        </p>\n"
            + "        <p>\n"
            + "            Para continuar el tramite realiza el pago y sigue las indicaciones del infograma.\n"
            + "        </p>\n"
            + "        <div>\n"
            + "            <a href=\"http://www.cuprex.mx/\">CUPREX</a>\n"
            + "            <a href=\"http://www.cuprex.mx/\">CUPREX Carrera</a>\n"
            + "        </div>\n"
            + "    </body>\n"
            + "</html>";

    public static String PAGO = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <title>Carrera CUPREX</title>\n"
            + "        <meta charset=\"UTF-8\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
            + "    </head>\n"
            + "    <body>\n"
            + "        <h3>Carrera CUPREX</h3>\n"
            + "        <p>       \n"
            + "            Estimado <b>||PERSONA</b> su pago a sido aceptado.\n"
            + "        </p>\n"
            + "        <p>\n"
            + "            Si aun no realizas el pago y te ha llegado este correo, por favor informalo a la organizacion del evento.\n"
            + "        </p>\n"
            + "        <p>\n"
            + "            Para continuar el tramite sigue las indicaciones del infograma.\n"
            + "        </p>\n"
            + "        <div>\n"
            + "            <a href=\"http://www.cuprex.mx/\">CUPREX</a>\n"
            + "            <a href=\"http://www.cuprex.mx/\">CUPREX Carrera</a>\n"
            + "        </div>\n"
            + "    </body>\n"
            + "</html>";

    public static void sendMail(ServletContext context, String msj, String recipient) throws Exception {

        if (SendMailTLS.email == null) {
            InputStream input = null;
            try {
                input = context.getResourceAsStream("/WEB-INF/email.properties");
                email = new Properties();
                email.load(input);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SendMailTLS.class.getName()).log(Level.SEVERE, "No se encuentra el archivo de propiedades de email", ex);
                email = null;
                throw new Exception("No se encuentra el archivo de propiedades de email");
            } catch (IOException ex) {
                Logger.getLogger(SendMailTLS.class.getName()).log(Level.SEVERE, "No se puede leer el archivo de propiedades de email", ex);
                email = null;
                throw new Exception("No se puede leer el archivo de propiedades de email");
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        String username = email.getProperty("user");
        String password = email.getProperty("pass");
        Boolean sendmails = Boolean.valueOf(email.getProperty("sendmails"));
        if (Boolean.FALSE.booleanValue() == sendmails) {
            Logger.getGlobal().log(Level.INFO, "El envio de correos esta desabilitado.");
            return;
        }

        if (username == null || password == null) {
            throw new Exception("No se encuentra el usuario o la contraseña");
        }

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject("Carrera CUPREX");
            message.setContent(msj, "text/html; charset=utf-8");

            Transport.send(message);
        } catch (MessagingException e) {
            Logger.getGlobal().log(Level.SEVERE, "Ocurrio un error al enviar el correo a " + recipient + ".\n" + e.getMessage());
            throw new Exception("Ocurrio un error al enviar el correo a " + recipient + ".");
        }
    }
}
