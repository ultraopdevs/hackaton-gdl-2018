package util;

import java.util.Collection;
import javax.ws.rs.PathParam;

public abstract class Resource<T, S> {

    protected String namedQueryRoot;

    /**
     *
     * @param s search data
     * @return number of row as string
     */
    public abstract String searchCount(S s);

    /**
     * Retrieves representation of an instance
     *
     * @param page page number starting on 1
     * @param qty rows per page
     * @param s search data
     * @return an instance
     */
    public abstract Collection<T> search(@PathParam("page") Integer page, @PathParam("qty") Integer qty, S s);

    /**
     * Retrieves current entries
     *
     * @return count of entries
     */
    public abstract String count();

    /**
     * Retrieves representation of an instance
     *
     * @param id
     * @return an instance
     */
    public abstract T getById(@PathParam("id") Integer id);

    /**
     * Retrieves representation of an instance
     *
     * @return a collection
     */
    public abstract Collection<T> getAll();

    /**
     * Retrieves representation of an instance
     *
     * @param page Page number
     * @param qty Number of results per page
     * @return a collection
     */
    public abstract Collection<T> getByPage(@PathParam("page") Integer page, @PathParam("qty") Integer qty);

    /**
     * POST method for updating an instance
     *
     * @param content
     * @return new identifier
     */
    public abstract String post(T content);

    /**
     * PUT method for updating or creating an instance
     *
     * @param id
     * @param content representation for the resource
     */
    public abstract void put(@PathParam("id") Integer id, T content);

    /**
     * DELETE method for updating an instance
     *
     * @param id
     */
    public abstract void delete(@PathParam("id") Integer id);

}
