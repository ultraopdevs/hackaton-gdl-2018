package util;

import com.google.gson.Gson;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable ex) {
        ex.printStackTrace();
        ErrorMessage errorMessage = new ErrorMessage();
        if (ex instanceof WebApplicationException) {
            WebApplicationException ex2 = (WebApplicationException) ex;
            errorMessage.setStatusCode(ex2.getResponse().getStatus());
            errorMessage.setApplicationMessage(ex2.getMessage());
            errorMessage.setStatusMessage(ex2.getResponse().getStatusInfo().getReasonPhrase());
        } else {
            errorMessage.setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()); //defaults to internal server error 500
            errorMessage.setStatusMessage(ex.getMessage());
        }

        Gson g = new Gson();
        Response.ResponseBuilder r = Response.status(errorMessage.getStatusCode());
        r = r.entity(g.toJson(errorMessage));
        r = r.type(MediaType.APPLICATION_JSON);
        Response rr = r.build();
        return rr;
    }
}
