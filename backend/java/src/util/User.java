package util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

class User {

    private static User adminUser = null;
    private static Properties adminUserProp = null;

    String username;
    String password;
    List<String> roles;
    Boolean active;

    private User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public boolean validate(String user, String pass) {
        return username != null && password != null && username.equals(user) && password.equals(pass);
    }

    public static User getAdminUser(ServletContext context) throws Exception {

        if (adminUser == null) {
            InputStream input = null;
            try {
                User u = new User();
                input = context.getResourceAsStream("/WEB-INF/admin.properties");
                adminUserProp = new Properties();
                adminUserProp.load(input);
                u.setUsername(adminUserProp.getProperty("user"));
                List<String> l = new ArrayList<>();
                l.add("ADMIN");
                u.setRoles(l);
                u.setActive(Boolean.valueOf(adminUserProp.getProperty("active")));
                u.setPassword(adminUserProp.getProperty("pass"));
                adminUser = u;
                return u;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SendMailTLS.class.getName()).log(Level.SEVERE, "No se encuentra el archivo de propiedades de admin", ex);
                adminUserProp = null;
                throw new Exception("No se encuentra el archivo de propiedades de admin");
            } catch (IOException ex) {
                Logger.getLogger(SendMailTLS.class.getName()).log(Level.SEVERE, "No se puede leer el archivo de propiedades de admin", ex);
                adminUserProp = null;
                throw new Exception("No se puede leer el archivo de propiedades de admin");
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return adminUser;
    }

}
