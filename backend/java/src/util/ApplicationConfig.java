package util;

import java.util.Set;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author diein
 */
@javax.ws.rs.ApplicationPath("ws")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        resources.add(RolesAllowedDynamicFeature.class);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(util.AuthenticationFilter.class);
        resources.add(util.CORSFilter.class);
        resources.add(util.GenericExceptionMapper.class);
        resources.add(ws.ReporteResource.class);
        resources.add(ws.SecurityResource.class);
        resources.add(ws.TransportadorResource.class);
        resources.add(ws.UnidadResource.class);
        resources.add(ws.UsuarioResource.class);
    }

}
