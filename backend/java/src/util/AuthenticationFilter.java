package util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.security.sasl.AuthenticationException;
import javax.servlet.ServletContext;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Context;

@Provider
@Priority(Priorities.AUTHENTICATION)
class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    private ServletContext scontext;

    private User userService;

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {

        try {
            User user = userService.getAdminUser(scontext);                              // (2)
            if (user.getActive()) {
                String authzHeader = crc.getHeaderString(HttpHeaders.AUTHORIZATION); // (1)
                if (authzHeader == null) {
                    throw new AuthenticationException("No authorizado - No hay credenciales de autentificacion");
                }
                if (!authzHeader.contains("Basic")) {
                    throw new AuthenticationException("No authorizado - El metodo de autnetificacion no es valido");
                }
                String decoded = new String(Base64.getDecoder().decode(authzHeader.substring("Basic".length()).trim()), Charset.forName("UTF-8"));
                String[] split = decoded.split(":");
                if (split.length < 2) {
                    throw new AuthenticationException("No authorizado - Las credenciales parecen estar incompletas");
                }
                if (user == null || !user.validate(split[0], split[1])) {    // (3)
                    throw new AuthenticationException("No authorizado - Las credenciales no son validas");
                }
            }

            SecurityContext oldContext = crc.getSecurityContext();               // (4)
            crc.setSecurityContext(new BasicSecurityContext(user, oldContext.isSecure()));
        } catch (Exception ex) {
            Logger.getLogger(AuthenticationFilter.class.getName()).log(Level.SEVERE, "Error al inicializar la seguridad", ex);
        }
    }
}
