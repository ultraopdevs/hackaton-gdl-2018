/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author diein
 */
@Entity
@Table(name = "unidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unidad.findAll", query = "SELECT u FROM Unidad u")
    , @NamedQuery(name = "Unidad.findByIdunidad", query = "SELECT u FROM Unidad u WHERE u.idunidad = :idunidad")
    , @NamedQuery(name = "Unidad.findByNumeroEconomico", query = "SELECT u FROM Unidad u WHERE u.numeroEconomico = :numeroEconomico")
    , @NamedQuery(name = "Unidad.findByPlacas", query = "SELECT u FROM Unidad u WHERE u.placas = :placas")
    , @NamedQuery(name = "Unidad.findByModelo", query = "SELECT u FROM Unidad u WHERE u.modelo = :modelo")
    , @NamedQuery(name = "Unidad.findByMarca", query = "SELECT u FROM Unidad u WHERE u.marca = :marca")
    , @NamedQuery(name = "Unidad.findByAnio", query = "SELECT u FROM Unidad u WHERE u.anio = :anio")
    , @NamedQuery(name = "Unidad.findByUltimoMantenimiento", query = "SELECT u FROM Unidad u WHERE u.ultimoMantenimiento = :ultimoMantenimiento")
    , @NamedQuery(name = "Unidad.findByPolizaSeguro", query = "SELECT u FROM Unidad u WHERE u.polizaSeguro = :polizaSeguro")
    , @NamedQuery(name = "Unidad.findByMaximoPasajeros", query = "SELECT u FROM Unidad u WHERE u.maximoPasajeros = :maximoPasajeros")
    , @NamedQuery(name = "Unidad.findByRuta", query = "SELECT u FROM Unidad u WHERE u.ruta = :ruta")})
public class Unidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idunidad")
    private Integer idunidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "numero_economico")
    private String numeroEconomico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "placas")
    private String placas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modelo")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "marca")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "anio")
    private String anio;
    @Column(name = "ultimo_mantenimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimoMantenimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "poliza_seguro")
    private String polizaSeguro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maximo_pasajeros")
    private int maximoPasajeros;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "ruta")
    private String ruta;
    @JoinColumn(name = "jefe_idjefe", referencedColumnName = "idjefe")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @XmlInverseReference(mappedBy = "jefe")
    private Jefe jefeIdjefe;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadIdunidad", fetch = FetchType.EAGER)
    private Collection<Reporte> reporteCollection;

    public Unidad() {
    }

    public Unidad(Integer idunidad) {
        this.idunidad = idunidad;
    }

    public Unidad(Integer idunidad, String numeroEconomico, String placas, String modelo, String marca, String anio, String polizaSeguro, int maximoPasajeros, String ruta) {
        this.idunidad = idunidad;
        this.numeroEconomico = numeroEconomico;
        this.placas = placas;
        this.modelo = modelo;
        this.marca = marca;
        this.anio = anio;
        this.polizaSeguro = polizaSeguro;
        this.maximoPasajeros = maximoPasajeros;
        this.ruta = ruta;
    }

    public Integer getIdunidad() {
        return idunidad;
    }

    public void setIdunidad(Integer idunidad) {
        this.idunidad = idunidad;
    }

    public String getNumeroEconomico() {
        return numeroEconomico;
    }

    public void setNumeroEconomico(String numeroEconomico) {
        this.numeroEconomico = numeroEconomico;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Date getUltimoMantenimiento() {
        return ultimoMantenimiento;
    }

    public void setUltimoMantenimiento(Date ultimoMantenimiento) {
        this.ultimoMantenimiento = ultimoMantenimiento;
    }

    public String getPolizaSeguro() {
        return polizaSeguro;
    }

    public void setPolizaSeguro(String polizaSeguro) {
        this.polizaSeguro = polizaSeguro;
    }

    public int getMaximoPasajeros() {
        return maximoPasajeros;
    }

    public void setMaximoPasajeros(int maximoPasajeros) {
        this.maximoPasajeros = maximoPasajeros;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Jefe getJefeIdjefe() {
        return jefeIdjefe;
    }

    public void setJefeIdjefe(Jefe jefeIdjefe) {
        this.jefeIdjefe = jefeIdjefe;
    }

    @XmlTransient
    public Collection<Reporte> getReporteCollection() {
        return reporteCollection;
    }

    public void setReporteCollection(Collection<Reporte> reporteCollection) {
        this.reporteCollection = reporteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idunidad != null ? idunidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unidad)) {
            return false;
        }
        Unidad other = (Unidad) object;
        if ((this.idunidad == null && other.idunidad != null) || (this.idunidad != null && !this.idunidad.equals(other.idunidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Unidad[ idunidad=" + idunidad + " ]";
    }
    
}
