/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author diein
 */
@Entity
@Table(name = "jefe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jefe.findAll", query = "SELECT j FROM Jefe j")
    , @NamedQuery(name = "Jefe.findByIdjefe", query = "SELECT j FROM Jefe j WHERE j.idjefe = :idjefe")})
public class Jefe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idjefe")
    private Integer idjefe;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jefeIdjefe", fetch = FetchType.EAGER)
    private Collection<Unidad> unidadCollection;
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuarioIdusuario;

    public Jefe() {
    }

    public Jefe(Integer idjefe) {
        this.idjefe = idjefe;
    }

    public Integer getIdjefe() {
        return idjefe;
    }

    public void setIdjefe(Integer idjefe) {
        this.idjefe = idjefe;
    }

    @XmlTransient
    public Collection<Unidad> getUnidadCollection() {
        return unidadCollection;
    }

    public void setUnidadCollection(Collection<Unidad> unidadCollection) {
        this.unidadCollection = unidadCollection;
    }

    public Usuario getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Usuario usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idjefe != null ? idjefe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jefe)) {
            return false;
        }
        Jefe other = (Jefe) object;
        if ((this.idjefe == null && other.idjefe != null) || (this.idjefe != null && !this.idjefe.equals(other.idjefe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Jefe[ idjefe=" + idjefe + " ]";
    }
    
}
