/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diein
 */
@Entity
@Table(name = "reporte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reporte.findAll", query = "SELECT r FROM Reporte r")
    , @NamedQuery(name = "Reporte.findByIdreporte", query = "SELECT r FROM Reporte r WHERE r.idreporte = :idreporte")
    , @NamedQuery(name = "Reporte.findByFecha", query = "SELECT r FROM Reporte r WHERE r.fecha = :fecha")
    , @NamedQuery(name = "Reporte.findByTipo", query = "SELECT r FROM Reporte r WHERE r.tipo = :tipo")
    , @NamedQuery(name = "Reporte.findByLatitud", query = "SELECT r FROM Reporte r WHERE r.latitud = :latitud")
    , @NamedQuery(name = "Reporte.findByLongitud", query = "SELECT r FROM Reporte r WHERE r.longitud = :longitud")
    , @NamedQuery(name = "Reporte.findByComentario", query = "SELECT r FROM Reporte r WHERE r.comentario = :comentario")})
public class Reporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idreporte")
    private Integer idreporte;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitud")
    private double latitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitud")
    private double longitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "comentario")
    private String comentario;
    @JoinColumn(name = "pasajero_idpasajero", referencedColumnName = "idpasajero")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Pasajero pasajeroIdpasajero;
    @JoinColumn(name = "transportador_idtransportador", referencedColumnName = "idtransportador")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Transportador transportadorIdtransportador;
    @JoinColumn(name = "unidad_idunidad", referencedColumnName = "idunidad")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Unidad unidadIdunidad;

    public Reporte() {
    }

    public Reporte(Integer idreporte) {
        this.idreporte = idreporte;
    }

    public Reporte(Integer idreporte, Date fecha, String tipo, double latitud, double longitud, String comentario) {
        this.idreporte = idreporte;
        this.fecha = fecha;
        this.tipo = tipo;
        this.latitud = latitud;
        this.longitud = longitud;
        this.comentario = comentario;
    }

    public Integer getIdreporte() {
        return idreporte;
    }

    public void setIdreporte(Integer idreporte) {
        this.idreporte = idreporte;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Pasajero getPasajeroIdpasajero() {
        return pasajeroIdpasajero;
    }

    public void setPasajeroIdpasajero(Pasajero pasajeroIdpasajero) {
        this.pasajeroIdpasajero = pasajeroIdpasajero;
    }

    public Transportador getTransportadorIdtransportador() {
        return transportadorIdtransportador;
    }

    public void setTransportadorIdtransportador(Transportador transportadorIdtransportador) {
        this.transportadorIdtransportador = transportadorIdtransportador;
    }

    public Unidad getUnidadIdunidad() {
        return unidadIdunidad;
    }

    public void setUnidadIdunidad(Unidad unidadIdunidad) {
        this.unidadIdunidad = unidadIdunidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idreporte != null ? idreporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reporte)) {
            return false;
        }
        Reporte other = (Reporte) object;
        if ((this.idreporte == null && other.idreporte != null) || (this.idreporte != null && !this.idreporte.equals(other.idreporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Reporte[ idreporte=" + idreporte + " ]";
    }
    
}
