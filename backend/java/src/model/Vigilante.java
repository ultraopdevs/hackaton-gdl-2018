/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author diein
 */
@Entity
@Table(name = "vigilante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vigilante.findAll", query = "SELECT v FROM Vigilante v")
    , @NamedQuery(name = "Vigilante.findByIdvigilante", query = "SELECT v FROM Vigilante v WHERE v.idvigilante = :idvigilante")})
public class Vigilante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idvigilante")
    private Integer idvigilante;
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuarioIdusuario;

    public Vigilante() {
    }

    public Vigilante(Integer idvigilante) {
        this.idvigilante = idvigilante;
    }

    public Integer getIdvigilante() {
        return idvigilante;
    }

    public void setIdvigilante(Integer idvigilante) {
        this.idvigilante = idvigilante;
    }

    public Usuario getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Usuario usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idvigilante != null ? idvigilante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vigilante)) {
            return false;
        }
        Vigilante other = (Vigilante) object;
        if ((this.idvigilante == null && other.idvigilante != null) || (this.idvigilante != null && !this.idvigilante.equals(other.idvigilante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Vigilante[ idvigilante=" + idvigilante + " ]";
    }
    
}
