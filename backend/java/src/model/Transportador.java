/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author diein
 */
@Entity
@Table(name = "transportador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transportador.findAll", query = "SELECT t FROM Transportador t")
    , @NamedQuery(name = "Transportador.findByIdtransportador", query = "SELECT t FROM Transportador t WHERE t.idtransportador = :idtransportador")
    , @NamedQuery(name = "Transportador.findByNombre", query = "SELECT t FROM Transportador t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "Transportador.findByApellido1", query = "SELECT t FROM Transportador t WHERE t.apellido1 = :apellido1")
    , @NamedQuery(name = "Transportador.findByApellido2", query = "SELECT t FROM Transportador t WHERE t.apellido2 = :apellido2")
    , @NamedQuery(name = "Transportador.findByDireccion", query = "SELECT t FROM Transportador t WHERE t.direccion = :direccion")
    , @NamedQuery(name = "Transportador.findByTelefono", query = "SELECT t FROM Transportador t WHERE t.telefono = :telefono")
    , @NamedQuery(name = "Transportador.findByGrupoSanguineo", query = "SELECT t FROM Transportador t WHERE t.grupoSanguineo = :grupoSanguineo")
    , @NamedQuery(name = "Transportador.findByLicenciaNumero", query = "SELECT t FROM Transportador t WHERE t.licenciaNumero = :licenciaNumero")
    , @NamedQuery(name = "Transportador.findByLicenciaFechaExpedicion", query = "SELECT t FROM Transportador t WHERE t.licenciaFechaExpedicion = :licenciaFechaExpedicion")
    , @NamedQuery(name = "Transportador.findByLicenciaFechaVencimiento", query = "SELECT t FROM Transportador t WHERE t.licenciaFechaVencimiento = :licenciaFechaVencimiento")
    , @NamedQuery(name = "Transportador.findByLicenciaTipo", query = "SELECT t FROM Transportador t WHERE t.licenciaTipo = :licenciaTipo")
    , @NamedQuery(name = "Transportador.findByCurp", query = "SELECT t FROM Transportador t WHERE t.curp = :curp")
    , @NamedQuery(name = "Transportador.findByObservaciones", query = "SELECT t FROM Transportador t WHERE t.observaciones = :observaciones")
    , @NamedQuery(name = "Transportador.findByNumeroEmergencia", query = "SELECT t FROM Transportador t WHERE t.numeroEmergencia = :numeroEmergencia")
    , @NamedQuery(name = "Transportador.findByDonadorOrganos", query = "SELECT t FROM Transportador t WHERE t.donadorOrganos = :donadorOrganos")})
public class Transportador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtransportador")
    private Integer idtransportador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 255)
    @Column(name = "apellido1")
    private String apellido1;
    @Size(max = 255)
    @Column(name = "apellido2")
    private String apellido2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "grupo_sanguineo")
    private String grupoSanguineo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "licencia_numero")
    private String licenciaNumero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "licencia_fecha_expedicion")
    @Temporal(TemporalType.DATE)
    private Date licenciaFechaExpedicion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "licencia_fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date licenciaFechaVencimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "licencia_tipo")
    private String licenciaTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "curp")
    private String curp;
    @Size(max = 2000)
    @Column(name = "observaciones")
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "numero_emergencia")
    private String numeroEmergencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "donador_organos")
    private short donadorOrganos;
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    //@XmlInverseReference(mappedBy = "usuario")
    private Usuario usuarioIdusuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transportadorIdtransportador", fetch = FetchType.EAGER)
    @XmlInverseReference(mappedBy = "report")
    private Collection<Reporte> reporteCollection;

    public Transportador() {
    }

    public Transportador(Integer idtransportador) {
        this.idtransportador = idtransportador;
    }

    public Transportador(Integer idtransportador, String nombre, String direccion, String telefono, String grupoSanguineo, String licenciaNumero, Date licenciaFechaExpedicion, Date licenciaFechaVencimiento, String licenciaTipo, String curp, String numeroEmergencia, short donadorOrganos) {
        this.idtransportador = idtransportador;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.grupoSanguineo = grupoSanguineo;
        this.licenciaNumero = licenciaNumero;
        this.licenciaFechaExpedicion = licenciaFechaExpedicion;
        this.licenciaFechaVencimiento = licenciaFechaVencimiento;
        this.licenciaTipo = licenciaTipo;
        this.curp = curp;
        this.numeroEmergencia = numeroEmergencia;
        this.donadorOrganos = donadorOrganos;
    }

    public Integer getIdtransportador() {
        return idtransportador;
    }

    public void setIdtransportador(Integer idtransportador) {
        this.idtransportador = idtransportador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public void setGrupoSanguineo(String grupoSanguineo) {
        this.grupoSanguineo = grupoSanguineo;
    }

    public String getLicenciaNumero() {
        return licenciaNumero;
    }

    public void setLicenciaNumero(String licenciaNumero) {
        this.licenciaNumero = licenciaNumero;
    }

    public Date getLicenciaFechaExpedicion() {
        return licenciaFechaExpedicion;
    }

    public void setLicenciaFechaExpedicion(Date licenciaFechaExpedicion) {
        this.licenciaFechaExpedicion = licenciaFechaExpedicion;
    }

    public Date getLicenciaFechaVencimiento() {
        return licenciaFechaVencimiento;
    }

    public void setLicenciaFechaVencimiento(Date licenciaFechaVencimiento) {
        this.licenciaFechaVencimiento = licenciaFechaVencimiento;
    }

    public String getLicenciaTipo() {
        return licenciaTipo;
    }

    public void setLicenciaTipo(String licenciaTipo) {
        this.licenciaTipo = licenciaTipo;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNumeroEmergencia() {
        return numeroEmergencia;
    }

    public void setNumeroEmergencia(String numeroEmergencia) {
        this.numeroEmergencia = numeroEmergencia;
    }

    public short getDonadorOrganos() {
        return donadorOrganos;
    }

    public void setDonadorOrganos(short donadorOrganos) {
        this.donadorOrganos = donadorOrganos;
    }

    public Usuario getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Usuario usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    public Collection<Reporte> getReporteCollection() {
        return reporteCollection;
    }

    public void setReporteCollection(Collection<Reporte> reporteCollection) {
        this.reporteCollection = reporteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransportador != null ? idtransportador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transportador)) {
            return false;
        }
        Transportador other = (Transportador) object;
        if ((this.idtransportador == null && other.idtransportador != null) || (this.idtransportador != null && !this.idtransportador.equals(other.idtransportador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Transportador[ idtransportador=" + idtransportador + " ]";
    }

}
