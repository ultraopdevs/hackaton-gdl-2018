package ws;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import model.Usuario;
import util.EntityManagerHelper;
import util.search.UsuarioSearch;

@Path("auth")
public class SecurityResource {

    @Context
    private UriInfo context;
    @Context
    private ServletContext scontext;

    public SecurityResource() {
    }

    private List<Predicate> buildPredicates(CriteriaBuilder cb, Root<Usuario> root, UsuarioSearch s) {
        List<Predicate> predicates = new ArrayList();
        Usuario c = s.getUsuario();
        predicates.add(cb.equal(root.<String>get("nombre"), c.getNombre()));
        predicates.add(cb.equal(root.<String>get("contrasena"), c.getContrasena()));
        return predicates;
    }

    @Path("login")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String login(UsuarioSearch s) {
        if (s == null || s.getUsuario() == null || s.getUsuario().getNombre() == null || s.getUsuario().getContrasena() == null) {
            throw new BadRequestException("Credenciales incompletas");
        }
        List<Usuario> l;
        Integer u = null;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
            Root<Usuario> root = cq.from(Usuario.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));
            TypedQuery<Usuario> q = e.createQuery(cq);
            l = q.getResultList();
            if (l.size() > 0 && l.get(0) != null) {
                u = l.get(0).getIdusuario();
            } else {
                throw new NotAuthorizedException("No se encuentra el usuario");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return u + "";
    }

    @Path("logout")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String logout() {
        return "";
    }

}
