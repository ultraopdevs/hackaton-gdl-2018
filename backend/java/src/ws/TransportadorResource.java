package ws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Transportador;
import util.EntityManagerHelper;
import util.Resource;
import util.SendMailTLS;
import util.search.TransportadorSearch;

@Path("transportador")
public class TransportadorResource extends Resource<Transportador, TransportadorSearch> {

    @Context
    private UriInfo context;
    @Context
    private ServletContext scontext;

    public TransportadorResource() {
        namedQueryRoot = "Transportador.";
    }

    @RolesAllowed("ADMIN")
    private List<Predicate> buildPredicates(CriteriaBuilder cb, Root<Transportador> root, TransportadorSearch s) {
        List<Predicate> predicates = new ArrayList();

        if (s.getTransportador() != null) {
            Transportador c = s.getTransportador();
            if (c.getIdtransportador() != null) {
                predicates.add(cb.equal(root.<String>get("idtransportador"), c.getIdtransportador()));
            }
            if (c.getNombre() != null) {
                predicates.add(cb.equal(root.<String>get("nombre"), c.getNombre()));
            }
            if (c.getApellido1() != null) {
                predicates.add(cb.equal(root.<String>get("apellido1"), c.getApellido1()));
            }
            if (c.getApellido2() != null) {
                predicates.add(cb.equal(root.<String>get("apellido2"), c.getApellido2()));
            }
        }

        return predicates;
    }

    @RolesAllowed("ADMIN")
    @Path("buscar/{pagina}/{qty}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Transportador> search(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty, TransportadorSearch s) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List<Transportador> l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Transportador> cq = cb.createQuery(Transportador.class);
            Root<Transportador> root = cq.from(Transportador.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));
            TypedQuery<Transportador> q = e.createQuery(cq);
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
            for (Transportador t : l) {
                t.setUsuarioIdusuario(null);
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("buscarConteo")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String searchCount(TransportadorSearch s) {
        String r;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Transportador> cq = cb.createQuery(Transportador.class);
            Root<Transportador> root = cq.from(Transportador.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));

            CriteriaQuery<Long> query = cb.createQuery(Long.class);
            query.select(cb.count(root));
            query.where(cq.getRestriction());
            root.alias(root.getAlias());
            query.distinct(cq.isDistinct());
            r = e.createQuery(query).getSingleResult().toString();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return r;
    }

    @RolesAllowed("ADMIN")
    @Path("conteo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String count() {
        Long o;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNativeQuery("select count(1) r from transportador");
            o = (Long) q.getSingleResult();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return o.intValue() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Transportador getById(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }
        Transportador cm;
        Transportador t = new Transportador();
        t.setIdtransportador(id);
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            cm = e.find(Transportador.class, id);
            cm.setUsuarioIdusuario(null);
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return cm;
    }

    @RolesAllowed("ADMIN")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Transportador> getAll() {
        List<Transportador> l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            l = q.getResultList();
            for (Transportador t : l) {
                t.setUsuarioIdusuario(null);
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("pagina/{pagina}/{qty}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Transportador> getByPage(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List<Transportador> l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
            for (Transportador t : l) {
                t.setUsuarioIdusuario(null);
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public void put(@PathParam("id") Integer id, Transportador content) {
        if (id == null) {
            throw new BadRequestException("Falta el numero");
        }
        if (content == null) {
            throw new BadRequestException("Faltan los datos para actualizacion");
        }
        if (content.getIdtransportador() == null) {
            throw new BadRequestException("Falta el numero en los datos");
        }
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Transportador oldData = e.find(Transportador.class, id);

            if (oldData == null) {
                throw new NotFoundException("No hay anterior para actualizar");
            }
            if (content.getIdtransportador().intValue() != oldData.getIdtransportador()) {
                throw new BadRequestException("El numero no coincide con el existente");
            }

            //Proteger relacion a otros datos, y datos sensibles
            try {
                e.getTransaction().begin();
                e.merge(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String post(Transportador content) {
        if (content == null) {
            throw new BadRequestException("No hay datos");
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            content.setIdtransportador(null);
            try {
                e.getTransaction().begin();
                e.persist(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                ex.printStackTrace();
                throw new InternalServerErrorException("");
            }
            try {
                SendMailTLS.sendMail(scontext, SendMailTLS.REGISTRO.replace("||PERSONA", content.getNombre() + " " + content.getApellido1() + " " + (content.getApellido2() == null ? content.getApellido2() : "")), "correo@gmail.com");
            } catch (Exception ex) {
                Logger.getLogger(TransportadorResource.class.getName()).log(Level.SEVERE, null, ex);
                throw new InternalServerErrorException("No se puede leer la informacion de envio de correos, este registro/pago no ha sido guardado");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return content.getIdtransportador() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @DELETE
    @Override
    public void delete(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Transportador cm = e.find(Transportador.class, id);

            if (cm == null) {
                throw new BadRequestException("No se encuentra");
            }

            try {
                e.getTransaction().begin();
                e.remove(cm);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }

    public int random(int min, int max) {
        return (int) (min + Math.round(Math.random() * (max - min)));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("actual")
    public String mockuptransportador() {
        return "{ \"id\": " + random(1, 9) + ",\"nom\": \"El Transportador 2\", \"ruta\": \"A-1\", \"numEco\": \"" + random(1, 89) + "\", \"likes\": " + random(1, 9000) + ", \"repo\": " + random(1, 50) + ", \"retraso\": " + random(-10, 10) + "}";
    }
}
