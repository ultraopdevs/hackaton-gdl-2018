package ws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Transportador;
import model.Usuario;
import util.EntityManagerHelper;
import util.Resource;
import util.SendMailTLS;
import util.search.UsuarioSearch;

@Path("usuario")
public class UsuarioResource extends Resource<Usuario, UsuarioSearch> {

    @Context
    private UriInfo context;
    @Context
    private ServletContext scontext;

    public UsuarioResource() {
        namedQueryRoot = "Usuario.";
    }

    @RolesAllowed("ADMIN")
    private List<Predicate> buildPredicates(CriteriaBuilder cb, Root<Usuario> root, UsuarioSearch s) {
        List<Predicate> predicates = new ArrayList();

        if (s.getUsuario() != null) {
            Usuario c = s.getUsuario();
            if (c.getIdusuario() != null) {
                predicates.add(cb.equal(root.<String>get("idusuario"), c.getIdusuario()));
            }
            if (c.getNombre() != null) {
                predicates.add(cb.equal(root.<String>get("nombre"), c.getNombre()));
            }
            if (c.getCorreo() != null) {
                predicates.add(cb.equal(root.<String>get("correo"), c.getCorreo()));
            }
        }

        return predicates;
    }

    @RolesAllowed("ADMIN")
    @Path("buscar/{pagina}/{qty}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Usuario> search(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty, UsuarioSearch s) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
            Root<Usuario> root = cq.from(Usuario.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));
            TypedQuery<Usuario> q = e.createQuery(cq);
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("buscarConteo")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String searchCount(UsuarioSearch s) {
        String r;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
            Root<Usuario> root = cq.from(Usuario.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));

            CriteriaQuery<Long> query = cb.createQuery(Long.class);
            query.select(cb.count(root));
            query.where(cq.getRestriction());
            root.alias(root.getAlias());
            query.distinct(cq.isDistinct());
            r = e.createQuery(query).getSingleResult().toString();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return r;
    }

    @RolesAllowed("ADMIN")
    @Path("conteo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String count() {
        Long o;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNativeQuery("select count(1) r from usuario");
            o = (Long) q.getSingleResult();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return o.intValue() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Usuario getById(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }
        Usuario cm;
        Usuario t = new Usuario();
        t.setIdusuario(id);
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            cm = e.find(Usuario.class, id);
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return cm;
    }

    @RolesAllowed("ADMIN")
    @Path("{id}/transportador")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Transportador getByTransportador(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }
        Usuario cm;
        Usuario t = new Usuario();
        t.setIdusuario(id);
        EntityManager e = EntityManagerHelper.getEntityManager();
        Transportador tt = null;
        try {
            cm = e.find(Usuario.class, id);
            if (cm != null && cm.getTransportadorCollection().iterator().hasNext()) {
                tt = cm.getTransportadorCollection().iterator().next();
                tt.setUsuarioIdusuario(null);
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return tt;
    }

    @RolesAllowed("ADMIN")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Usuario> getAll() {
        List<Usuario> l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            l = q.getResultList();

        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("pagina/{pagina}/{qty}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Usuario> getByPage(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public void put(@PathParam("id") Integer id, Usuario content) {
        if (id == null) {
            throw new BadRequestException("Falta el numero");
        }
        if (content == null) {
            throw new BadRequestException("Faltan los datos para actualizacion");
        }
        if (content.getIdusuario() == null) {
            throw new BadRequestException("Falta el numero en los datos");
        }
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Usuario oldData = e.find(Usuario.class, id);

            if (oldData == null) {
                throw new NotFoundException("No hay anterior para actualizar");
            }
            if (content.getIdusuario().intValue() != oldData.getIdusuario()) {
                throw new BadRequestException("El numero no coincide con el existente");
            }

            //Proteger relacion a otros datos, y datos sensibles
            content.setNombre(oldData.getNombre());
            try {
                e.getTransaction().begin();
                e.merge(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String post(Usuario content) {
        if (content == null) {
            throw new BadRequestException("No hay datos");
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            content.setIdusuario(null);
            try {
                e.getTransaction().begin();
                e.persist(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                ex.printStackTrace();
                throw new InternalServerErrorException("");
            }
            try {
                SendMailTLS.sendMail(scontext, SendMailTLS.REGISTRO.replace("||PERSONA", content.getNombre()), "correo@gmail.com");
            } catch (Exception ex) {
                Logger.getLogger(UsuarioResource.class.getName()).log(Level.SEVERE, null, ex);
                throw new InternalServerErrorException("No se puede leer la informacion de envio de correos, este registro/pago no ha sido guardado");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return content.getIdusuario() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @DELETE
    @Override
    public void delete(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Usuario cm = e.find(Usuario.class, id);

            if (cm == null) {
                throw new BadRequestException("No se encuentra");
            }

            try {
                e.getTransaction().begin();
                e.remove(cm);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }
}
