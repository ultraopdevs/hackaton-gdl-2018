package ws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Unidad;
import util.EntityManagerHelper;
import util.Resource;
import util.search.UnidadSearch;

@Path("unidad")
public class UnidadResource extends Resource<Unidad, UnidadSearch> {

    @Context
    private UriInfo context;
    @Context
    private ServletContext scontext;

    public UnidadResource() {
        namedQueryRoot = "Unidad.";
    }

    @RolesAllowed("ADMIN")
    private List<Predicate> buildPredicates(CriteriaBuilder cb, Root<Unidad> root, UnidadSearch s) {
        List<Predicate> predicates = new ArrayList();

        if (s.getUnidad() != null) {
            Unidad c = s.getUnidad();
            if (c.getIdunidad() != null) {
                predicates.add(cb.equal(root.<String>get("idunidad"), c.getIdunidad()));
            }
        }

        return predicates;
    }

    @RolesAllowed("ADMIN")
    @Path("buscar/{pagina}/{qty}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Unidad> search(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty, UnidadSearch s) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Unidad> cq = cb.createQuery(Unidad.class);
            Root<Unidad> root = cq.from(Unidad.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));
            TypedQuery<Unidad> q = e.createQuery(cq);
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("buscarConteo")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String searchCount(UnidadSearch s) {
        String r;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            CriteriaBuilder cb = e.getCriteriaBuilder();
            CriteriaQuery<Unidad> cq = cb.createQuery(Unidad.class);
            Root<Unidad> root = cq.from(Unidad.class);

            List<Predicate> predicates = buildPredicates(cb, root, s);

            cq.select(root).where(predicates.toArray(new Predicate[]{}));

            CriteriaQuery<Long> query = cb.createQuery(Long.class);
            query.select(cb.count(root));
            query.where(cq.getRestriction());
            root.alias(root.getAlias());
            query.distinct(cq.isDistinct());
            r = e.createQuery(query).getSingleResult().toString();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return r;
    }

    @RolesAllowed("ADMIN")
    @Path("conteo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String count() {
        Long o;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNativeQuery("select count(1) r from unidad");
            o = (Long) q.getSingleResult();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return o.intValue() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Unidad getById(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }
        Unidad cm;
        Unidad t = new Unidad();
        t.setIdunidad(id);
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            cm = e.find(Unidad.class, id);
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return cm;
    }

    @RolesAllowed("ADMIN")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Unidad> getAll() {
        List<Unidad> l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            l = q.getResultList();

        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("pagina/{pagina}/{qty}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<Unidad> getByPage(@PathParam("pagina") Integer page, @PathParam("qty") Integer qty) {
        if (page == null || qty == null || page < 1 || qty < 1) {
            throw new BadRequestException();
        }
        List l;
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Query q = e.createNamedQuery(namedQueryRoot + "findAll");
            q.setFirstResult((page - 1) * qty);
            q.setMaxResults(qty);
            l = q.getResultList();
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return l;
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public void put(@PathParam("id") Integer id, Unidad content) {
        if (id == null) {
            throw new BadRequestException("Falta el numero");
        }
        if (content == null) {
            throw new BadRequestException("Faltan los datos para actualizacion");
        }
        if (content.getIdunidad() == null) {
            throw new BadRequestException("Falta el numero en los datos");
        }
        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Unidad oldData = e.find(Unidad.class, id);

            if (oldData == null) {
                throw new NotFoundException("No hay anterior para actualizar");
            }
            if (content.getIdunidad().intValue() != oldData.getIdunidad()) {
                throw new BadRequestException("El numero no coincide con el existente");
            }

            //Proteger relacion a otros datos, y datos sensibles
            content.setJefeIdjefe(oldData.getJefeIdjefe());
            try {
                e.getTransaction().begin();
                e.merge(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String post(Unidad content) {
        if (content == null) {
            throw new BadRequestException("No hay datos");
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            content.setIdunidad(null);
            try {
                e.getTransaction().begin();
                e.persist(content);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                ex.printStackTrace();
                throw new InternalServerErrorException("");
            }

        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
        return content.getIdunidad() + "";
    }

    @RolesAllowed("ADMIN")
    @Path("{id}")
    @DELETE
    @Override
    public void delete(@PathParam("id") Integer id) {
        if (id == null) {
            throw new BadRequestException();
        }

        EntityManager e = EntityManagerHelper.getEntityManager();
        try {
            Unidad cm = e.find(Unidad.class, id);

            if (cm == null) {
                throw new BadRequestException("No se encuentra");
            }

            try {
                e.getTransaction().begin();
                e.remove(cm);
                e.getTransaction().commit();
            } catch (RollbackException ex) {
                throw new InternalServerErrorException("");
            }
        } finally {
            if (e.getTransaction().isActive()) {
                e.getTransaction().rollback();
            }
            EntityManagerHelper.closeEntityManager();
        }
    }
}
